import React from 'react'
import {shallow} from 'enzyme'
import Aux from '../../Aux/Aux'

it('renders the children', () => {
    const wrapped = shallow(
        <Aux>
            <h1>React</h1>
        </Aux>
    )

    expect(wrapped.find('h1').length).toEqual(1)
})