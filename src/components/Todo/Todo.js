import React from 'react';

import Aux from '../../hoc/Aux/Aux'
import TodoList from './TodoList/TodoList'
import TodoInput from './TodoInput/TodoInput'

const todo = () => (
    <Aux>
        <TodoInput/>
        <TodoList/>
    </Aux>
)

export default todo