import React from 'react'
import {mount} from 'enzyme'
import TodoItem from '../../../../Todo/TodoList/TodoItem/TodoItem'

import Checkbox from '@material-ui/core/Checkbox'

let wrapped

beforeEach(() => {
    wrapped = mount(<TodoItem todo="Buy some milk" completed={true}/>)
})

it('renders the text', () => {
    expect(wrapped.text()).toContain('Buy some milk')
})

it('checked the checkbox when the todo is completed', () => {
    expect(wrapped.find(Checkbox).props().checked).toBe(true)
})