import React from 'react'
import {mount} from 'enzyme'
import TodoList from '../../../Todo/TodoList/TodoList'

import TodoItem from '../../../Todo/TodoList/TodoItem/TodoItem'

import {createStore} from 'redux'
import {Provider} from 'react-redux'
import reducers from '../../../../store/reducers'

let wrapped

beforeEach(() => {
    wrapped = mount(
        <Provider store={createStore(reducers, {todos: [{text: 'Buy some milk', completed: false}, {text: 'Buy some beer', completed: true}, {text: 'Buy some juice', completed: false}]})}>
            <TodoList/>
        </Provider>
    )
})

it('renders the list items', () => {
    expect(wrapped.find(TodoItem).length).toEqual(3)
})
