import React from 'react'
import {mount} from 'enzyme'
import TodoInput from '../../../Todo/TodoInput/TodoInput'

import TextField from '@material-ui/core/TextField'

import {createStore} from 'redux'
import {Provider} from 'react-redux'
import reducers from '../../../../store/reducers'

let wrapped

beforeEach(() => {
    wrapped = mount(
        <Provider store={createStore(reducers, {})}>
            <TodoInput/>
        </Provider>
    )
})

it('renders a material textfield', () => {
    expect(wrapped.find(TextField).length).toEqual(1)
})

it('clears the input after the user submit a todo', () => {
    wrapped.find('input').simulate('change', {
        target: {value: 'Buy some milk'}
    })

    wrapped.find('form').simulate('submit')

    expect(wrapped.find(TextField).prop('value')).toEqual('')
})