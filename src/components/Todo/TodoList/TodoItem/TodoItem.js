import React from 'react'

import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import Checkbox from '@material-ui/core/Checkbox'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import classes from './TodoItem.css'

const todoItem = props => (
    <ListItem divider>
        <ListItemText className={props.completed ? classes.completed : null} primary={props.todo}/>
        <ListItemSecondaryAction>
            <Checkbox checked={props.completed} onChange={props.onChecked}/>
            <IconButton onClick={props.onDelete} aria-label="Delete">
                <DeleteIcon/>
            </IconButton>
        </ListItemSecondaryAction>
    </ListItem>
)

export default todoItem