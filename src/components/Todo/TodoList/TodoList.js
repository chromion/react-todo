import React, {Component} from 'react';
import {completeTodo, deleteTodo} from '../../../store/actions'
import {connect} from 'react-redux'

import List from '@material-ui/core/List'
import TodoItem from './TodoItem/TodoItem'

class TodoList extends Component {
    render() {
        return (
            <List>
                {this.props.todos.map((todo, index) => <TodoItem key={index} todo={todo.text} completed={todo.completed} onChecked={() => this.props.completeTodo(index)} onDelete={() => this.props.deleteTodo(index)}/>)}
            </List>
        )
    }
}

function mapStateToProps(state) {
    return {
        todos: state.todos
    }
}

export default connect(mapStateToProps, {completeTodo, deleteTodo})(TodoList)