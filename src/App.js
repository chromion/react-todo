import React from 'react'

import Layout from './hoc/Layout/Layout'
import Todo from './components/Todo/Todo'


const app = () => (
    <Layout>
        <Todo/>
    </Layout>
)

export default app