import React, {Component} from 'react'
import {connect} from 'react-redux'
import {addTodo} from '../../../store/actions'

import TextField from '@material-ui/core/TextField'

class TodoInput extends Component {
    state = {
        todo: ''
    }

    handleSubmit = event => {
        event.preventDefault()
        const {todo} = this.state
        if(todo) {
            this.props.addTodo(this.state.todo)
            this.setState({todo: ''})
        }
    }

    updateTodo = event => {
        this.setState({todo: event.target.value})
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <TextField label="Add to your todo list..." fullWidth value={this.state.todo} onChange={this.updateTodo}/>
            </form>
        )
    }
}

export default connect(null, {addTodo})(TodoInput)