import React from 'react'
import {shallow} from 'enzyme'
import Todo from '../../Todo/Todo'

import TodoInput from '../../Todo/TodoInput/TodoInput'
import TodoList from '../../Todo/TodoList/TodoList'

let wrapped

beforeEach(() => {
    wrapped = shallow(<Todo/>)
})

it('renders todo input component', () => {
    expect(wrapped.find(TodoInput).length).toEqual(1)
})

it('renders todo list component', () => {
    expect(wrapped.find(TodoList).length).toEqual(1)
})