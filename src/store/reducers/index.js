import {combineReducers} from 'redux'
import {ADD_TODO, COMPLETE_TODO, DELETE_TODO} from '../actions'

const initialState = []

const todos = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO:
            return [...state, {text: action.todo, completed: false}]
        case COMPLETE_TODO:
            let todos = [...state]
            todos[action.id].completed = !todos[action.id].completed
            return todos
        case DELETE_TODO:
            return state.filter((_, index) => index !== action.id)
        default:
            return state
    }
}

const rootReducer = combineReducers({
    todos
})

export default rootReducer