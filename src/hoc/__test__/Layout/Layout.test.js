import React from 'react'
import {shallow} from 'enzyme'
import Layout from '../../Layout/Layout'

it('renders the children', () => {
    const wrapped = shallow(
        <Layout>
            <h1>React</h1>
        </Layout>
    )

    expect(wrapped.find('h1').length).toEqual(1)
})